from pydantic import BaseModel

class Usuario(BaseModel):
    id: int
    name: str
    surname: str
    correo: str

    class Config:
        orm_mode = True
