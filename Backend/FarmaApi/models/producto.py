from typing import List
from pydantic import BaseModel, Field

class Categoria(BaseModel):
    id: str = Field(description="Identificador único de la categoría")
    tipo_categoria: str = Field(description="Tipo de categoría del medicamento")
    nombre: str = Field(description="Nombre de categoría del medicamento")
    descripcion: str = Field(description="Descripción de la categoría")
    imagen_categoria: str = Field(description="Ruta de la imagen de la categoría")

class Oferta(BaseModel):
    id: str = Field(description="Identificador único de la Oferta")
    medicamento_id: str = Field(description="Id del medicamento") 
    precio_descuento:float = Field(description="Precio final de la Oferta")

class Medicamento(BaseModel):
    id: str = Field(description="Identificador único del medicamento")  # Usando str porque en el JSON es "001"
    categoria_id: str = Field(description="ID de la categoría del medicamento")
    nombre_comercial: str = Field(description="Nombre comercial del medicamento")
    nombre_generico: str = Field(description="Nombre genérico del medicamento")
    forma_farmaceutica: str = Field(description="Forma farmacéutica del medicamento")
    concentracion: str = Field(description="Concentración del medicamento")
    via_administracion: str = Field(description="Vía de administración del medicamento")
    laboratorio: str = Field(description="Nombre del laboratorio que fabrica el medicamento")
    lote: str = Field(description="Número de lote del medicamento")
    fecha_vencimiento: str = Field(description="Fecha de vencimiento del medicamento")
    registro_sanitario: str = Field(description="Registro sanitario del medicamento")
    condicion_venta: str = Field(description="Condición de venta del medicamento")
    precio: float = Field(description="Precio de venta del medicamento")
    stock_disponible: int = Field(description="Cantidad disponible en stock")
    codigo_barras: str = Field(description="Código de barras del medicamento")
    pais_origen: str = Field(description="País de origen del medicamento")
    precauciones: str = Field(description="Precauciones a tener en cuenta")
    imagen_coleccion: List[str] = Field(description="Colección de imágenes relacionadas con el medicamento")
    reacciones_adversas: List[str] = Field(description="Lista de reacciones adversas del medicamento")
    indicaciones: str = Field(description="Indicaciones del medicamento")
    contraindicaciones: List[str] = Field(description="Lista de contraindicaciones del medicamento")
    uso_durante_embarazo: str = Field(description="Uso durante el embarazo")
    condiciones_almacenamiento: str = Field(description="Condiciones de almacenamiento del medicamento")
    imagen: str = Field(description="Ruta de la imagen del medicamento")
    formula: str = Field(description="Formula del medicamento")
    accion_terapeutica: str = Field(description="Acción terapéutica")
    via_administracion_dosis: str = Field(description="Vida de administración y dosis")
    dosis_presentacion: str = Field(description="Dosis de presentación")
