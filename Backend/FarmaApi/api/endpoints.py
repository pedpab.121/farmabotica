import os
from FarmaApi.api import schemas
from FarmaApi.models.usuario import Usuario
from FarmaApi.models.producto import Medicamento, Categoria, Oferta

from fastapi import APIRouter, Body, HTTPException, Query
import json

router = APIRouter()

urlBase_files = os.path.abspath("data")
# urlBase_files = 'Backend/FarmaApi/data'
dataUser_path = os.path.join(os.path.dirname(__file__), '../data/dataUsers.json')
dataProduct_path = os.path.join(os.path.dirname(__file__), '../data/productos.json')
dataCategoria_path = os.path.join(os.path.dirname(__file__), '../data/categorias.json')
dataOferta_path = os.path.join(os.path.dirname(__file__), '../data/ofertas.json')

# Carga inicial de datos desde dataUsers.json
with open(dataUser_path , "r", encoding='utf-8') as f:
    data = json.load(f)
    usuarios = [Usuario(**usuario) for usuario in data]


# Carga inicial de datos desde productos.json
with open(dataProduct_path, "r", encoding='utf-8') as f:
    data = json.load(f)
    medicamentos = [Medicamento(**medicamento) for medicamento in data]
    
# Carga inicial de datos desde categorias.json
with open(dataCategoria_path, "r", encoding='utf-8') as f:
    data = json.load(f)
    categorias = [Categoria(**categoria) for categoria in data]
    
# Carga inicial de datos desde ofertas.json
with open(dataOferta_path, "r", encoding='utf-8') as f:
    data = json.load(f)
    ofertas = [Oferta(**oferta) for oferta in data]

@router.get("/saludo")
def saludo():
  return "Hola Mundo"
# Endpoint GET para obtener todos los medicamentos ()
@router.get("/medicamentos", response_model=list[Medicamento])
def get_medicamentos(category: str = '', page: int = 1, limit: int = Query(None)):

    # Filtrar por categoría si se pasa una categoría
    if len(category) > 0:
        medicamentos_filtrados = [med for med in medicamentos if med.categoria_id == category]
    else:
        medicamentos_filtrados = medicamentos
    
    # Si no se pasa un límite o el límite es 0, devolver todos los resultados
    if limit is None or limit == 0:
        return medicamentos_filtrados
    
    # Calcular inicio y fin de acuerdo a la paginación
    inicio = (page - 1) * limit
    fin = inicio + limit
    
    return medicamentos_filtrados[inicio:fin]

# Endpoint GET para obtener todos los medicamentos
@router.get("/medicamentos_by_id/{id}", response_model= Medicamento)
def get_medicamentos_by_Id(id: str):
    medicamento = next((med for med in medicamentos if med.id == id), None)

    if medicamento is None:
        raise HTTPException(status_code=404, detail="Medicamento no encontrado")

    return medicamento

# Endpoint GET para obtener todos los medicamentos por termino
@router.get("/medicamentos_by_termino", response_model=list[Medicamento])
def get_medicamentos_by_termino(termino: str = '', page: int = 1, limit: int = Query(None)):
    termino = termino.lower()  # Convertir el término a minúsculas para hacer la búsqueda insensible a mayúsculas
    resultados = []

    # Filtrar medicamentos buscando en todos los campos relevantes
    for medicamento in medicamentos:
        if (
            termino in medicamento.nombre_comercial.lower() or
            termino in medicamento.nombre_generico.lower() or
            termino in medicamento.forma_farmaceutica.lower() or
            termino in medicamento.concentracion.lower() or
            termino in medicamento.via_administracion.lower() or
            termino in medicamento.laboratorio.lower()
        ):
            resultados.append(medicamento)
    if limit is None or limit == 0:
        return resultados
    
    # Calcular inicio y fin de acuerdo a la paginación
    inicio = (page - 1) * limit
    fin = inicio + limit
    
    return resultados[inicio:fin]

# Endpoint GET para obtener todos las Ofertas
@router.get("/ofertas", response_model=list[Oferta])
def get_ofertas(limit: int = Query(None)):
    # Load medicamentos from dataUsers.json
    if limit is None or limit == 0:
        return ofertas  # Retorna todas las ofertas si no se especifica un límite o si el límite es 0
    return ofertas[:limit]

# Endpoint GET para obtener todos las Categorías
@router.get("/categorias", response_model=list[Categoria])
def get_categorias():
    # Load categorias from categorias.json
    return categorias

# Endpoint GET para obtener todos los usuarios
@router.get("/usuarios", response_model=list[schemas.UsuarioRead])
def get_users():
    return usuarios

# Endpoint GET para obtener un usuario por ID
@router.get("/usuarios/{usuario_id}", response_model=schemas.UsuarioRead)
def get_user_by_id(usuario_id: int):
    usuario = next((u for u in usuarios if u.id == usuario_id), None)
    if not usuario:
        raise HTTPException(status_code=404, detail="Usuario no encontrado")
    return usuario

# Endpoint POST para agregar un nuevo usuario
@router.post("/usuarios", response_model=schemas.UsuarioRead)
def add_user(usuario: schemas.UsuarioCreate = Body(...)):
    # Validar datos usando el esquema UsuarioCreate
    nuevo_usuario = Usuario(**usuario.dict())

    #validamos si Id existe
    if len(usuarios) > 0 :
        ultimo_usuario = usuarios[-1].id
        nuevo_usuario.id = ultimo_usuario + 1
    else:
        nuevo_usuario = 1
    
    usuarios.append(nuevo_usuario)

    # Actualizar dataUsers.json con el nuevo usuario
    with open(urlBase_files + "/dataUsers.json", "w") as f:
        json.dump([usuario.dict() for usuario in usuarios], f, indent=4)

    return nuevo_usuario

# Endpoint DELETE para eliminar un usuario por ID
@router.delete("/usuarios/{usuario_id}")
def remove_user(usuario_id: int):
    usuario = next((u for u in usuarios if u.id == usuario_id), None)
    if not usuario:
        raise HTTPException(status_code=404, detail="Usuario no encontrado")

    usuarios.remove(usuario)

    # Actualizar dataUsers.json sin el usuario eliminado
    with open(urlBase_files + "/dataUsers.json", "w") as f:
        json.dump([usuario.dict() for usuario in usuarios], f, indent=4)

    return {"message": "Usuario eliminado exitosamente"}
