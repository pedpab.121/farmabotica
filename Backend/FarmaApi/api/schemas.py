from FarmaApi.models.usuario import Usuario
from pydantic import validator

class UsuarioBase(Usuario):
    pass

class UsuarioCreate(UsuarioBase):
    @validator("correo", pre=True)
    def validar_correo(cls, correo):
        if not "@" or not correo.endswith(".com"):
            raise ValueError("El correo electrónico debe ser un correo Valido")
        return correo

class UsuarioRead(UsuarioBase):
    pass
