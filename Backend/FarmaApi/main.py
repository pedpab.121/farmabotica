from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from FarmaApi.api.endpoints import router

app = FastAPI()

origins = ["http://localhost:8989", "http://localhost:5500", "https://farmalabotica.vercel.app"]
# Ajusta los orígenes

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(router)

# if __name__ == "__main__":
#     import uvicorn
#     uvicorn.run(app, host="127.0.0.1", port=8000)

# if __name__ == "__main__":
#     import uvicorn
#     port = int(os.getenv("PORT", 8000))
#     uvicorn.run("main:app", host="0.0.0.0", port=port)