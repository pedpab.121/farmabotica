// Variables para almacenar respuestas de las llamadas API
let medicamentosResponse = {};
let categoriaResponse = [];
let ofertasResponse = [];

// Configuración de Swiper para el slider principal de inicio
const swiperHome = new Swiper(".mySwiperHome", {
    speed: 1500,
    autoplay: { delay: 3000, disableOnInteraction: false },
    loop: true,
    navigation: { nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev" },
    effect: 'fade',
});

// Evento para aplicar animaciones AOS en el cambio de slide
swiperHome.on('slideChange', () => {
    const activeSlide = swiperHome.slides[swiperHome.activeIndex];
    const slideElements = activeSlide.querySelectorAll('[data-aos]');
    slideElements.forEach(element => AOS.init(element));
});

const swiperOfertas = new Swiper('.mySwiperOfertas', {
    slidesPerView: 3, // Permite que los slides ocupen espacio automático
    spaceBetween: 15, // Espacio entre los slides
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    scrollbar: {
        draggable: true,
        el: '.swiper-scrollbar',
        hide: false, // Establece esto en false para asegurarte de que la barra de desplazamiento esté visible
    },
    breakpoints: {
        1500: { slidesPerView: 4 },
        // 1200: { slidesPerView: 3 },
        992: { slidesPerView: 3 },
        767: { slidesPerView: 2 },
        320: { slidesPerView: 1 },
    },
    freeMode: true, // Permite el desplazamiento libre
});

// Configuración del slider de categorías
const swiperCategories = new Swiper('.product-category-slider', {
    slidesPerView: 3,
    spaceBetween: 20,
    pagination: { el: '.product-category-slider .swiper-pagination', clickable: true },
    breakpoints: {
        1500: { slidesPerView: 3 },
        1200: { slidesPerView: 3 },
        767: { slidesPerView: 2 },
        320: { slidesPerView: 1 },
    }
});

$(document).ready(async () => {
    // Cargar datos de medicamentos, categorías y ofertas
    mostrar_loading();
    [medicamentosResponse, categoriaResponse, ofertasResponse] = await Promise.all([
        getMedicamentos("", 0),
        getCategorias(),
        getOfertas(5),
    ]);
    setTimeout(() => {
        ocultar_loading();
    }, 1000);
    // Renderizar categorías, medicamentos y ofertas según los datos recibidos
    if (categoriaResponse.length) renderCategoria();
    if (ofertasResponse.length) await renderOfertas(processOfertas());

    // Tooltip en hover
    $('.tooltip-container').hover(function () {
        $(this).find('.tooltip-text').toggle();
    });
});

// Función para procesar ofertas y combinar datos de medicamentos
function processOfertas() {
    return ofertasResponse.map(oferta => {
        const medicamento = medicamentosResponse.find(med => med.id === oferta.medicamento_id.toString());
        return medicamento ? { ...oferta, ...medicamento } : oferta;
    });
}

// Renderizar categorías en el slider
function renderCategoria() {
    const slideCat = document.querySelector("#categorias #slide_categoria");
    if (!slideCat) return;

    slideCat.innerHTML = '';
    const swiperWrapper = document.createElement('div');
    swiperWrapper.className = 'swiper-wrapper';

    categoriaResponse.forEach(categoria => {
        const imgSrc = categoria.imagen_categoria.includes("http") ? categoria.imagen_categoria : `${categoria.imagen_categoria}` || './img/anticonceptivos.jpg';
        const slide = `
            <div class="swiper-slide">
                <div class="card">
                    <div class="card-header">
                        <img src="${imgSrc}" alt="${categoria.nombre}">
                        <h3>${categoria.nombre}</h3>
                    </div>
                    <div class="card-body">
                        <div class="tooltip-container">
                            <p class="truncated-description">${categoria.descripcion || 'Descripción no disponible.'}</p>
                            <span class="tooltip-text">${categoria.descripcion || 'Descripción no disponible.'}</span>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="catalogo.html" class="btn btn-primary text-white btn-size mt-3">Catálogo Ahora</a>
                    </div>
                </div>
            </div>
        `;
        swiperWrapper.insertAdjacentHTML('beforeend', slide);
    });

    slideCat.appendChild(swiperWrapper);
    slideCat.insertAdjacentHTML('beforeend', '<div class="swiper-pagination"></div>');
    // Configuración del slider de categorías
    const swiperCategories = new Swiper('.product-category-slider', {
        slidesPerView: 3,
        spaceBetween: 15,
        pagination: { el: '#slide_categoria .swiper-pagination', clickable: true },
        breakpoints: {
            1440: { slidesPerView: 4 },
            992: { slidesPerView: 3 },
            767: { slidesPerView: 2 },
            320: { slidesPerView: 1 },
        }
    });
    swiperCategories.update();
}

// Renderizar ofertas en el contenedor correspondiente
function renderOfertas(ofertas) {
    const slideOfertas = document.querySelector("#ofertas_destacadas .mySwiperOfertas");
    if (!slideOfertas) return;

    slideOfertas.innerHTML = '';
    const swiperWrapper = document.createElement('div');
    swiperWrapper.className = 'swiper-wrapper';

    ofertas.forEach(oferta => {
        const slide = `
          <div class="swiper-slide">
                                    <div class="card mb-3 card-oferta">
                                        <div class="card-img-top position-relative">
                                             <img src="${oferta.imagen || './img/anticonceptivos.jpg'}" class="card-img-top" alt="${oferta.nombre_generico}">
                                            <div class="sale-ribbon position-absolute">
                                                <span class="text-white font-weight-bold">Oferta!</span>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                             <h5 class="card-title">${oferta.nombre_comercial}</h5>
                                            <div class="d-flex justify-content-between">
                                                <div class="text-muted text-body-tertiary"><del>${oferta.precio} Bs</del></div>
                        <div class="text-primary font-weight-bold texto-precios">${oferta.precio_descuento} Bs</div>
                                            </div>
                                             <a href="interna.html?id=${oferta.categoria_id}" class="btn btn-primary btn-size mt-3">Ver detalle</a>
                                        </div>
                                    </div>
                                </div>
        `;

        swiperWrapper.insertAdjacentHTML('beforeend', slide);
    });
    slideOfertas.appendChild(swiperWrapper);
    slideOfertas.insertAdjacentHTML('beforeend', '<div class="swiper-scrollbar"></div>');

    const swiperOfertas = new Swiper('.mySwiperOfertas', {
        slidesPerView: 3, // Permite que los slides ocupen espacio automático
        spaceBetween: 15, // Espacio entre los slides
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        scrollbar: {
            draggable: true,
            el: '.mySwiperOfertas .swiper-scrollbar',
            hide: false, // Establece esto en false para asegurarte de que la barra de desplazamiento esté visible
        },
        breakpoints: {
            1500: { slidesPerView: 4 },
            // 1200: { slidesPerView: 3 },
            992: { slidesPerView: 3 },
            767: { slidesPerView: 2 },
            320: { slidesPerView: 1 },
        },
        freeMode: true, // Permite el desplazamiento libre
    });
    swiperOfertas.update();
}
