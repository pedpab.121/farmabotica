let urlParams = new URLSearchParams(window.location.search);
let medicamento_id = urlParams.get('id') || "";
let categorias = [];
let detalle_medicamento = {};

$(document).ready(async () => {
    mostrar_loading()
    categorias = await getCategorias();
    if (medicamento_id) {
        await medicamento_by_id(medicamento_id);
    }
    setTimeout(() => {
        ocultar_loading()
    }, 1000);
});

const medicamento_by_id = async (id) => {
    detalle_medicamento = await get_medicamento_by_id(id);
    if (detalle_medicamento) {
        renderSlider();
        renderDetalle(detalle_medicamento);
    }
}

const renderSlider = () => {
    const slideDetail = document.querySelector(".mySwiper_detalle");
    const thumbsSlider = document.querySelector(".mySwiper");

    if (!slideDetail || !thumbsSlider) return; // Validación en una sola línea

    slideDetail.innerHTML = '<div class="swiper-wrapper"></div>';
    thumbsSlider.innerHTML = '<div class="swiper-wrapper"></div>';

    const swiperWrapperDetail = slideDetail.querySelector(".swiper-wrapper");
    const swiperWrapperThumbs = thumbsSlider.querySelector(".swiper-wrapper");

    detalle_medicamento.imagen_coleccion.forEach((imagen, index) => {
        const slide = `<div class="swiper-slide"><img src="${imagen}" alt="Imagen ${index + 1}" /></div>`;
        swiperWrapperDetail.insertAdjacentHTML('beforeend', slide);
        swiperWrapperThumbs.insertAdjacentHTML('beforeend', slide);
    });

    slideDetail.insertAdjacentHTML('beforeend', '<div class="swiper-button-next"></div><div class="swiper-button-prev"></div>');

    // Actualiza Swiper
    const swiperThumbs = new Swiper(".mySwiper", {
        spaceBetween: 10,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesProgress: true,
    });
    new Swiper(".mySwiper_detalle", {
        spaceBetween: 10,
        navigation: { nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev" },
        thumbs: { swiper: swiperThumbs },
    });
};

const renderDetalle = ({ nombre_comercial, nombre_generico, concentracion, forma_farmaceutica, via_administracion, categoria_id, formula, indicaciones, contraindicaciones, reacciones_adversas, via_administracion_dosis, dosis_presentacion , laboratorio}) => {
    document.querySelector("#title_page").textContent = nombre_comercial;
    document.querySelector(".sumary-title").textContent = `${nombre_comercial} (${nombre_generico}) ${concentracion}`;

    const encontrada = categorias.find(c => c.id === categoria_id);
    document.querySelector('.descripcion').textContent = `${forma_farmaceutica} (${concentracion}) administración ${via_administracion}  ${laboratorio !== '' ? '| ' + laboratorio : ''}`;
    const categoriaLink = document.querySelector('#categoria');
    categoriaLink.textContent = encontrada ? encontrada.nombre : '';
    categoriaLink.href = encontrada ? `catalogo.html?categoria=${encontrada.id}` : '#';

    document.querySelector('#card-formula .card-text').textContent = formula;
    document.querySelector('#card-indicaciones .card-text').textContent = indicaciones;

    const fillList = (selector, items) => {
        const container = document.querySelector(selector);
        container.innerHTML = '';
        items.forEach(item => container.insertAdjacentHTML('beforeend', `<li>${item}</li>`));
    };

    fillList('#contraindicaciones .card-text', contraindicaciones);
    fillList('#reacciones .card-text', reacciones_adversas);

    document.querySelector('#administracion .card-text').textContent = via_administracion_dosis;
    document.querySelector('#dosis .card-text').textContent = dosis_presentacion;
};
