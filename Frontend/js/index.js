(async () => {
  await loadHeaderFooter();
  $(document).ready(() => {
    $(".search-open").click(() => {
      let input = document.querySelector('.search-wrap #search-input');
      $(".search-wrap").toggleClass('active');
      input.focus();
      document.getElementById('search-input').addEventListener('keyup', (e) => {
        if (e.key === 'Enter') {
          buscarMedicamento();
        }
      });
    });

    $(".search-close").click(() => {
      $("#search-input").val('');
      $(".search-wrap").toggleClass('active');
    });
  });

  generarMapGoogle();
  AOS.init();
  actualizarNavLinks();
})();



async function loadHeaderFooter() {
  const headerContainer = document.getElementById('header');
  const footerContainer = document.getElementById('footer');
  try {
    const [headerResponse, footerResponse] = await Promise.all([
      fetch('header.html'),
      fetch('footer.html')
    ]);

    if (!headerResponse.ok || !footerResponse.ok) {
      throw new Error('Failed to load header or footer');
    }

    const [headerHtml, footerHtml] = await Promise.all([
      headerResponse.text(),
      footerResponse.text()
    ]);

    headerContainer.innerHTML = headerHtml;
    footerContainer.innerHTML = footerHtml;
  } catch (error) {
    console.error('Error loading header or footer:', error);
  }
}


function generarMapGoogle() {
  const directions = [
    "Avenida Los Leones 77-27, Rubio 5030, Táchira, Venezuela",
    "Calle Principal San Diego 14-56, Rubio 5030, Táchira, Venezuela",
    "Calle Principal, Parroquia Bramón, Municipio Junín, Táchira, Venezuela"
  ];
  document.getElementById("map-link").href = createMapLink(directions);
}

function createMapLink(directions) {
  const baseUrl = "https://www.google.com/maps/";
  const markers = directions.map(direction => `markers=${encodeURIComponent(direction)}`).join('&');
  return `https://www.google.com/maps/dir/Centro+Comercial+San+Diego,+PJ2P%2B9R4,+Rubio+5030,+T%C3%A1chira/77+La+Botica+Tu+Farmacia,+Final,+77-27+Av.+Los+Leones,+Rubio+5030,+T%C3%A1chira/Bram%C3%B3n,+5029,+T%C3%A1chira/@7.6720259,-72.3888749,13.25z/data=!4m20!4m19!1m5!1m1!1s0x8e663e44455a25d7:0xf917479d149afa13!2m2!1d-72.3629334!2d7.7008961!1m5!1m1!1s0x8e663f12e59dbe19:0x833eb05c66308626!2m2!1d-72.3500858!2d7.6959587!1m5!1m1!1s0x8e663c394bebcc2d:0x385d70ad597444cc!2m2!1d-72.395927!2d7.65762!3e0?entry=ttu&g_ep=EgoyMDI0MTAyMy4wIKXMDSoASAFQAw%3D%3D`;
}

function buscarMedicamento() {
  const terminoBusqueda = document.getElementById("search-input").value;
  if (terminoBusqueda) {
    window.location.href = `catalogo.html?busqueda=${terminoBusqueda}`;
  }
}

function actualizarNavLinks() {
  const navLinks = document.querySelectorAll('#header nav ul li.nav-item a');
  const currentPath = location.href.includes('?') ? location.href.split('?')[0] : location.href;

  navLinks.forEach(link => {
    if (link.href === currentPath || (link.href.includes('catalogo') && currentPath.includes('interna'))) {
      link.parentElement.classList.add('active');
    } else {
      link.parentElement.classList.remove('active');
    }
  });
}

function mostrar_loading() {
  document.querySelector("#loading").classList.remove("loading-none")
}
function ocultar_loading() {
  document.querySelector("#loading").classList.add("loading-none")
}
