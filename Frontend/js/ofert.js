ofertas_lista

$(document).ready(async () => {

    mostrar_loading();
    [medicamentosResponse, categoriaResponse, ofertasResponse] = await Promise.all([
        getMedicamentos("", 0),
        getCategorias(),
        getOfertas(0),
    ]);
    setTimeout(() => {
        ocultar_loading();
    }, 1000);

    // Renderizar categorías, medicamentos y ofertas según los datos recibidos
    // if (categoriaResponse.length) renderCategoria();
    if (ofertasResponse.length) await renderOfertas(processOfertas());
})
// Renderizar ofertas en el contenedor correspondiente
function renderOfertas(ofertas) {
    const ofertas_lista = document.querySelector("#ofertas_lista");


    ofertas_lista.innerHTML = '';


    ofertas.forEach((ofer,i) => {
        let precio_descuento = ofer.precio - ofer.precio_descuento;
        const oferta = `
        <div class="card card-ofertas">
            <div class="badge-oferta">
                <div class="${i%2 ?  "flash" :"flash out"}">
                    <div class="oct-callout__first-line"> ${i%2 ? "NUEVA" : "SE VA PRONTO"}</div>
                </div>
            </div>
            <div class="card-image">
                <img class="img-fluid" src="${ofer.imagen}" alt="${ofer.nombre_generico}">
            </div>
            <div class="card-body px-0">
                <div class="card-paragraph mb-3">
                    Descuento ${(((ofer.precio - precio_descuento) / ofer.precio) * 100).toFixed(2)}%
                </div>
                
            </div>
        </div>

        
        `;
        // <div class="btn btn-link p-0 position-absolute bottom right">
        //             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
        //                 fill="currentColor" class="bi bi-box-arrow-in-up-right" viewBox="0 0 16 16">
        //                 <path fill-rule="evenodd"
        //                     d="M6.364 13.5a.5.5 0 0 0 .5.5H13.5a1.5 1.5 0 0 0 1.5-1.5v-10A1.5 1.5 0 0 0 13.5 1h-10A1.5 1.5 0 0 0 2 2.5v6.636a.5.5 0 1 0 1 0V2.5a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v10a.5.5 0 0 1-.5.5H6.864a.5.5 0 0 0-.5.5" />
        //                 <path fill-rule="evenodd"
        //                     d="M11 5.5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793l-8.147 8.146a.5.5 0 0 0 .708.708L10 6.707V10.5a.5.5 0 0 0 1 0z" />
        //             </svg>
        //         </div>

        ofertas_lista.innerHTML += oferta ;
    });

}
// Función para procesar ofertas y combinar datos de medicamentos
function processOfertas() {
    return ofertasResponse.map(oferta => {
        const medicamento = medicamentosResponse.find(med => med.id === oferta.medicamento_id.toString());
        return medicamento ? { ...oferta, ...medicamento } : oferta;
    });
}