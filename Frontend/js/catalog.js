// Variables globales
let categoriaResponse = [];
let productosCache = null;
let page = 1;
const limit = 10;
let term = '';
let addedQuery = false;

/**
 * Función ready de js 
 */
$(document).ready(async () => {
    mostrar_loading();
    const urlParams = new URLSearchParams(window.location.search);
    const categoriaId = urlParams.get('categoria');
    const busqueda = urlParams.get('busqueda');

    categoriaResponse = await getCategorias();
    if (categoriaResponse && categoriaResponse.length > 0) {
        renderCategoria();
        mostrarLoading();

        if (busqueda !== null) {
            await busquedaTermino(busqueda);
        } else {
            await cargarProductoPorCategoria(categoriaId || "", page);
        }

        ocultarLoading();
    }
    setTimeout(() => {
        ocultar_loading();
    }, 1000);
});

// Mostrar/ocultar el menú desplegable
document.querySelector('.dropdown-categoria .dropdown-toggle').addEventListener('click', () => {
    const dropdownMenu = document.querySelector('.dropdown-categoria #dropdown-menu');
    dropdownMenu.style.display = dropdownMenu.style.display === 'none' ? 'block' : 'none';
});

// Función para cargar más productos al hacer clic en el botón
document.getElementById('loadMoreBtn').addEventListener('click', async () => {
    page++;  // Aumentar la página
    mostrarLoading();
    let medicamentos = term ? await getMedicamentosByTerminos(term, page, limit) : await getMedicamentos("", page, limit);

    if (medicamentos.length === 0) {
        mostrarMensajeSinMasResultados();
    } else {
        addedQuery = true;
        renderMedicamentos(addedQuery, medicamentos);
    }
    ocultarLoading();
});

// Función para buscar productos al presionar 'Enter'
document.getElementById('input_search_term').addEventListener('keyup', async (e) => {
    if (e.key === 'Enter' && e.target.value !== '') {
        var newUrl = window.location.origin + window.location.pathname; // Base URL
        // If you want to add other parameters, you can do it here
        // newUrl += '?page=2';
    
        // Use history.pushState to add a new entry to the history
        history.pushState({}, '', newUrl);
        mostrarLoading();
        await busquedaTermino(e.target.value);
        ocultarLoading();
    }
});

// Funciones auxiliares

async function busquedaTermino(termino) {
    term = termino;
    const medicamentos = await getMedicamentos_by_terminos(termino.toString(), 1, limit);

    if (medicamentos.length === 0) {
        mostrarMensajeSinMasResultados();
        const productsContainer = document.getElementById('products-container');
        productsContainer.classList.remove("product-grid");
        productsContainer.innerHTML = '<div class="none-products"> <p> No se  encontraron resultados </p></div>';
    } else {
        document.getElementById('loadMoreBtn').style.display = medicamentos.length < limit ? 'none' : 'block';
        addedQuery = false;
        renderMedicamentos(addedQuery, medicamentos);
        location.hash = "#product-list"
    }
}

function mostrarMensajeSinMasResultados() {
    const btn = document.getElementById('loadMoreBtn');
    const noMore = document.getElementById('no-more');

    btn.style.display = 'none';
    noMore.style.display = 'block';

    setTimeout(() => {
        noMore.style.display = 'none';
    }, 1000);

    term = '';
}

function mostrarLoading() {
    const loading = document.querySelector('.content-loading');
    loading.innerHTML = `
        <div class="spinner-border" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
        <span role="status">Loading...</span>
    `;
}

function ocultarLoading() {
    document.querySelector('.content-loading').innerHTML = '';
}

const renderCategoria = () => {
    let menuContainer = document.documentElement.querySelector("#menu-container");
    menuContainer.innerHTML = '';
    let itemAll = document.createElement("div");
    itemAll.classList = "menu-item active";
    itemAll.id = `item-0`;
    let enlace = document.createElement('a');
    enlace.href = "javascript:void(0);"
    enlace.setAttribute("data-category", "")
    // Asignar el evento onclick usando addEventListener
    enlace.addEventListener('click', async function () {
        // Puedes obtener la categoría desde el atributo del div padre
        const category = enlace.getAttribute('data-category');
        mostrarLoading()
        document.querySelector("#products-container").innerHTML = '';
        await cargarProductoPorCategoria(category, 1);
        setTimeout(() => {
            ocultarLoading()

        }, 1500);
    });
    enlace.textContent = "Todas";
    itemAll.appendChild(enlace);
    menuContainer.appendChild(itemAll);


    categoriaResponse.forEach((categoria) => {
        const childCategoria = document.createElement('div');
        childCategoria.className = "menu-item";
        childCategoria.id = `item-${categoria.id}`;
        let enlace = document.createElement('a');
        enlace.href = "javascript:void(0);"
        enlace.setAttribute("data-category", categoria.id)
        // Asignar el evento onclick usando addEventListener
        enlace.addEventListener('click', async function () {
            document.querySelector("#products-container").innerHTML = '';
            mostrarLoading()
            // Puedes obtener la categoría desde el atributo del div padre
            const category = enlace.getAttribute('data-category');
            await cargarProductoPorCategoria(category, 1);
            setTimeout(() => {
                ocultarLoading()

            }, 1500);
        });
        enlace.textContent = categoria.nombre;
        childCategoria.appendChild(enlace);
        menuContainer.appendChild(childCategoria);
    })

}

const cargarProductoPorCategoria = async (categoria, pageN) => {
    term = ""
    // Seleccionar todos los elementos de menú, incluyendo los del dropdown
    document.querySelectorAll("#menu-container .menu-item, #dropdown-categoria .menu-item").forEach((item) => {
        item.classList.remove('active'); // Remover la clase 'active' de todos los elementos
    });

    // Seleccionar el ítem correspondiente en el menú o dropdown que no esten ocultos
    let link = document.querySelector(`#menu-container #item-${categoria}:not(.hidden), #dropdown-categoria #item-${categoria}:not(.hidden)`);
    if (categoria !== '') {
        if (link) {
            link.classList.add('active'); // Agregar la clase 'active' al elemento correspondiente
        }
    } else {
        if (link) {
            link.classList.add('active'); // Manejar el caso cuando 'categoria' está vacío
        } else {
            document.querySelector(`#menu-container #item-0`).classList.add('active');
        }
    }
    const dropdownMenu = document.querySelector('.dropdown-categoria #dropdown-menu');
    dropdownMenu.style.display = dropdownMenu.style.display = 'none';
    // var productosFiltrados = [];
    productosCache = await getMedicamentos(categoria, pageN, limit); // Guardar en caché
    if (productosCache.length === 0 && addedQuery) {
        let btn = document.getElementById('loadMoreBtn')
        btn.style.display = 'none';
        let noMore = document.getElementById("no-more");
        noMore.style.display = "block"
        setTimeout(() => {
            noMore.style.display = "none"
        }, 1000);

    } else if (productosCache.length < limit) {
        let btn = document.getElementById('loadMoreBtn')
        btn.style.display = 'none';
    } else {
        let btn = document.getElementById('loadMoreBtn')
        btn.style.display = 'block';
    }

    productosFiltrados = productosCache; // categoria !== "0" ? productosCache.filter(p => p.categoria_id === categoria) : productosCache;

    // Mostrar productos filtrados en la interfaz
    renderMedicamentos(false, productosFiltrados)
}
const renderMedicamentos = (add, medicamentos) => {
    const productsContainer = document.getElementById('products-container');
    if (medicamentos && medicamentos.length > 0) {
        productsContainer.classList = "product-grid"
        if (!add) {
            productsContainer.innerHTML = '';
        }
        var children = '';
        // <p>${medicamento.laboratorio}</p>
        medicamentos.forEach((medicamento) => {
            children += `
                <div class="product-card">
                <div class="img-content">
                    <img class="img-fluid" src="${medicamento.imagen}" alt="${medicamento.nombre_generico}">
                </div>
            <div class="product-info">
                <h3>${medicamento.nombre_comercial}</h3>
                <p>${medicamento.forma_farmaceutica} ${medicamento.concentracion} | ${medicamento.via_administracion}</p>
               
                <p class="price">${medicamento.precio} Bs</p>
                <a href="interna.html?id=${medicamento.id}" class="btn btn-primary text-white btn-size mt-3">Ver detalle</a>
            </div>
        </div>
                `
        })
        productsContainer.innerHTML += children;
    } else {
        if (medicamentos.length === 0 && !add) {
            console.log(productsContainer)
            productsContainer.classList.remove("product-grid");
            productsContainer.innerHTML = '<div class="none-products"> <p> No se  encontraron resultados </p></div>';
        }
    }
    addedQuery = false
}
// Función para manejar el menú desplegable cuando los elementos no caben
const adjustMenu = () => {
    const menuContainer = document.getElementById('menu-container');
    const dropdownMenu = document.getElementById('dropdown-menu');
    const dropdown = document.getElementById('dropdown-categoria');
    const menuItems = Array.from(menuContainer.children).filter(item => !item.classList.contains('dropdown'));
    const maxMenuWidth = menuContainer.clientWidth;
    let totalWidth = 0;

    // Resetear la visibilidad de los elementos del menú
    menuItems.forEach(item => {
        item.classList.remove('hidden');
    });

    // Resetear el contenido del desplegable
    dropdownMenu.innerHTML = '';
    dropdown.style.display = 'none';

    // Recorrer los elementos y ocultar los que no caben
    menuItems.forEach(item => {
        totalWidth += item.offsetWidth;
        if (totalWidth > maxMenuWidth) {
            item.classList.add('hidden'); // Ocultar el elemento
            const clone = item.cloneNode(true); // Clonar para el desplegable
            clone.classList.remove('hidden');
            const enlace = clone.querySelector('a');
            if (enlace) {
                enlace.addEventListener('click', async function () {
                    document.querySelector("#products-container").innerHTML = '';
                    mostrarLoading()
                    const category = enlace.getAttribute('data-category');
                    await cargarProductoPorCategoria(category, 1);
                    setTimeout(() => {
                        ocultarLoading()
                    }, 1500);
                });
            }
            dropdownMenu.appendChild(clone);
            dropdown.style.display = 'flex';
        }
    });
}


// Observador de redimensionamiento para ajustar el menú en tiempo real
const resizeObserver = new ResizeObserver(adjustMenu);

// Adjuntar el observador al contenedor del menú
resizeObserver.observe(document.getElementById('menu-container'));


// Ajustar el menú al cargar la página
window.addEventListener('load', adjustMenu);