        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();

        $(document).ready(function () {
            $('#delete').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var item = button.data('title')
                var modal = $(this)
                modal.find('.modal-title').text(item)
            })
           
            $('.icons').click(function () {
                $(this).toggleClass('iconck').siblings().removeClass('iconck')
            })
            $('#turnbf').click(function () {
                $('#turnbf span').addClass('turn')
            })
            $('.turnaf').click(function () {
                $('.turnaf span').toggleClass('turnb')
            })
        });

        $("#next-step1").click(function() {
            $("#step1").toggleClass('active')
            $("#step2").toggleClass('active')
        })
        $("#prev-step1").click(function() {
            $("#step1").toggleClass('active')
            $("#step2").toggleClass('active')
        })
        $("#next-step3").click(function() {
            $("#step2").toggleClass('active')
            $("#step3").toggleClass('active')
        })

        $("#btn-backHome").click(function() {
            $("#step1").toggleClass('active')
            $("#step3").toggleClass('active')
        })