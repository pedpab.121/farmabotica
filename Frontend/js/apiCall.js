const URL_BASE_BACKEND = config.API_URL_BASE // "https://farmabotica-api.vercel.app/"
/**
 * @description Obtiene Todos los medicamentos
 * @returns 
 */
const getMedicamentos = async (cat ='', page = 1, limit =0) => {
    try {
        const response = await fetch(URL_BASE_BACKEND + `medicamentos?category=${cat}&page=${page}&limit=${limit}`); // Espera la respuesta de la API

        if (!response.ok) {
            throw new Error('Network response was not ok'); // Lanza un error si la respuesta no es válida
        }

        const data = await response.json(); // Espera que se convierta la respuesta a JSON
        return data; // Retorna los datos
    } catch (error) {
        console.error('Hubo un problema con la solicitud Fetch:', error); // Maneja el error
        throw error; // Lanza el error para que se pueda manejar más arriba si es necesario
    }

}
const getMedicamentos_by_terminos = async (term ='', page = 1, limit =0) => {
    try {
        const response = await fetch(URL_BASE_BACKEND + `medicamentos_by_termino?termino=${term}&page=${page}&limit=${limit}`); // Espera la respuesta de la API

        if (!response.ok) {
            throw new Error('Network response was not ok'); // Lanza un error si la respuesta no es válida
        }

        const data = await response.json(); // Espera que se convierta la respuesta a JSON
        return data; // Retorna los datos
    } catch (error) {
        console.error('Hubo un problema con la solicitud Fetch:', error); // Maneja el error
        throw error; // Lanza el error para que se pueda manejar más arriba si es necesario
    }

}
const get_medicamento_by_id = async (id) => {
    try {
        const response = await fetch(URL_BASE_BACKEND + `medicamentos_by_id/${id}`); // Espera la respuesta de la API

        if (!response.ok) {
            throw new Error('Network response was not ok'); // Lanza un error si la respuesta no es válida
        }

        const data = await response.json(); // Espera que se convierta la respuesta a JSON
        return data; // Retorna los datos
    } catch (error) {
        console.error('Hubo un problema con la solicitud Fetch:', error); // Maneja el error
        throw error; // Lanza el error para que se pueda manejar más arriba si es necesario
    }

}
/**
 * @description Obtiene Todas las categorías de Medicamentos  
 * @returns 
 */
const getCategorias = async () => {
    try {
        const response = await fetch(URL_BASE_BACKEND + "categorias"); // Espera la respuesta de la API

        if (!response.ok) {
            throw new Error('Network response was not ok'); // Lanza un error si la respuesta no es válida
        }

        const data = await response.json(); // Espera que se convierta la respuesta a JSON
        return data; // Retorna los datos
    } catch (error) {
        console.error('Hubo un problema con la solicitud Fetch:', error); // Maneja el error
        throw error; // Lanza el error para que se pueda manejar más arriba si es necesario
    }
};

const getOfertas = async (limit) => {
    try {
        const response = await fetch(URL_BASE_BACKEND + `ofertas?limit=${limit}`); // Espera la respuesta de la API

        if (!response.ok) {
            throw new Error('Network response was not ok'); // Lanza un error si la respuesta no es válida
        }

        const data = await response.json(); // Espera que se convierta la respuesta a JSON
        return data; // Retorna los datos
    } catch (error) {
        console.error('Hubo un problema con la solicitud Fetch:', error); // Maneja el error
        throw error; // Lanza el error para que se pueda manejar más arriba si es necesario
    }
};