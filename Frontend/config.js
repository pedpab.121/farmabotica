// config.js
const config = {
    API_URL_BASE: window.location.hostname === 'localhost' ?
        "http://127.0.0.1:8000/" :
        "https://farmabotica-api.vercel.app/"
    // URL de desarrollo local
};

// Exportar para módulos de ES6 o directamente acceder como `config.apiUrl`
